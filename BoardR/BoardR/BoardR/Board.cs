﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoardR
{
    class Board
    {
        public static List<BoardItem> items = new List<BoardItem>();

        public static void AddItem(BoardItem boardItem)
        {
            if (items.Contains(boardItem))
            {
                throw new ArgumentException("Already exist!");
            }
            items.Add(boardItem);
            

        }
        public static int TotalItems
        {
            get
            {
                return items.Count;
            }
        }


    }
}
