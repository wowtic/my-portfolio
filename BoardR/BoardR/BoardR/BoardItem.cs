﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BoardR
{
    class BoardItem
    {
        private string _title;
        private DateTime _dueDate;
        private Status _status;
        private List<EventLog> log = new List<EventLog>();

        public BoardItem(string title, DateTime dueDate)
        {
            if (title.Length < 5 || title.Length > 30)
            {
                throw new ArgumentException("Title length should be 5 to 30");
            }
            if (title == null)
            {
                throw new ArgumentNullException("Title cannot be null");
            }
            this._title = title;
            if (dueDate < DateTime.Now)
            {
                throw new ArgumentException("DueDate cannot be in the past!");
            }
            this._dueDate = dueDate;
            this._status = Status.Open;
            this.log.Add(new EventLog($"Item created {this.ViewInfo()}"));
            
        }

        public string Title
        {
            get
            {
                return this._title;
            }
            set
            {
                if (value.Length < 5 || value.Length > 30)
                {
                    throw new ArgumentException("Title length should be 5 to 30");
                }
                if (value == null)
                {
                    throw new ArgumentNullException("Title cannot be null");
                }
                this.log.Add(new EventLog($"Title changed from {this._title} to {value}"));

                _title = value;
            }
        }

        public DateTime DueDate
        {
            get
            {
                return this._dueDate;
            }
            set
            {
                if (value < DateTime.Now)
                {
                    throw new ArgumentException("DueDate cannot be in the past!");
                }
                this.log.Add(new EventLog($"DueDate changed from {this._dueDate.ToString("dd-MM-yyyy")} to {value.ToString("dd-MM-yyyy")}"));

                this._dueDate = value;
            }
        }

        public Status Status
        {
            get
            {
                return this._status;
            }
            private set
            {
                value = this._status;
            }
        }

        public void AdvanceStatus()
        {
            if (this._status == Status.Verified)
            {
                return;
            }
            Status previousStatus = this.Status;

            this._status++;
            this.log.Add(new EventLog($"Status changed from {previousStatus} to {this._status}"));
        }
        public void RevertStatus()
        {
            if (this._status == Status.Open)
            {
                return;
            }
            Status previousStatus = this.Status;


            this._status--;
            this.log.Add(new EventLog($"Status changed from {previousStatus} to {this._status}"));
        }

        public string ViewInfo()
        {
            return $"'{this._title}', [{this._status}|{this._dueDate.ToString("dd-MM-yyyy")}]";
        }

        public string ViewHistory()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in this.log)
            {
                sb.AppendLine(item.ViewInfo());
            }
            return sb.ToString();
        }
    }
}
