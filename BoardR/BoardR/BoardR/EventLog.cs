﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoardR
{
    class EventLog
    {
        private string description;
        private DateTime time;
        private static List<EventLog> log = new List<EventLog>();

        public EventLog(string description)
        {
            this.Description = description;
            this.Time = DateTime.Now;
        }

        public string Description 
        {
            get => description; 
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Description cannot be null.");
                }
                description = value;
            }
                
        }
        public DateTime Time { get => time; set => time = value; }
        internal List<EventLog> Log { get => log; private set => log = value; }

        public void Add(EventLog log)
        {
            this.Log.Add(log);
        }
        public string ViewInfo()
        {
            return $"[{this.Time.ToString("yyyyMMdd|hh:mm:ffff")}]{this.Description}";
        }
    }
}
