﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BoardR
{
    enum Status
    {
        Open,
        Todo,
        InProgress,
        Done,
        Verified
    }
}
